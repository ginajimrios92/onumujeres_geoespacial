#
# Authors: OE, GJ
# Maintainers: OE, GJ
# Copytight: Data Cívica © 2021, GPL v2 or newer
# --------------------------------------------------
# onumujeres_geoespacial/clean-data/src/clean-data.R

if(!require(pacman)) install.packages("pacman")
pacman::p_load(tidyverse, stringr, stringi, data.table, here)

files <- list(sinais = here("import/output/sinais.rds"),
              censo_viv = here("import/output/viviendas.rds"),
              censo_per = here("import/output/personas.rds"),
              rnpdno = here("import/output/rnpdno.rds"),
              militares = here("import/output/militares.rds"),
              incidencia = here("import/output/incidencia.rds"),
              materna = here("import/output/materna.rds"),
              denue_text = here("import/output/denue_text.rds"),
              denue_min = here("import/output/denue_mine.rds"),
              denue_agro = here("import/output/denue_agro.rds"),
              conflictos = here("import/output/conflictos.rds"),
              centros_atencion = here("import/output/centros-atencion.rds"),
              carreteras = here("import/output/acces-carretera.rds"),
              pob_mun = here("import/input/pob-mun.rds"),
              nom_mun = here("import/input/nom-mun.rds"),
              output = here("clean-data/output/violencia-muj.rds")
              )

clean_text <- function(s){
  str_squish(toupper(stri_trans_general(s, "latin-ascii")))
}

# ---- Población CONAPO ----
pob_mun <- readRDS(files$pob_mun) %>% 
  filter(year==2019) %>% 
  group_by(inegi, sexo) %>% 
  summarise(pob = sum(pob_mun, na.rm = T)) %>% 
  ungroup() %>% 
  pivot_wider(names_from = sexo, values_from = pob) %>% 
  rename(pobH = Hombres,
         pobM = Mujeres) %>% 
  left_join(readRDS(files$nom_mun) %>% 
              select(-c(cve_ent, cve_mun))) %>% 
  select(inegi, nom_ent, nom_mun, pobH, pobM)

# ---- SINAIS ----
pattern_cancer <- "TUMOR MALIGNO|TUMORES MALIGNOS|MELANOMA MALIGNO|MELANOMAS MALIGNOS|MESOTELIOMA|SARCOMA|SARCOMAS|CARCINOMA|CARCINOMAS|CARCINOIDE|LINFOMA|LINFOMAS|MICOSIS FUNGOIDE|TRANSTORNOS LINFOPROLIFERATIVOS|MIELODISPLASICOS|MIELOPROLIFERATIVA|LEUCEMIA|LEUCEMIAS|ENFERMEDAD DE SEZARY|ENFERMEDAD DE CADENA PESADA"

sinais <- readRDS(files$sinais) %>% 
  filter(inegi != "99999" & str_sub(inegi, 3, 5) != "999" & sexo!=9) %>% 
  mutate(causa = clean_text(causa),
         tipo = case_when(causa %like% pattern_cancer ~ "cancer",
                          presunto==2 ~ "homicidio",
                          presunto==3 ~ "suicidio",
                          T ~ NA_character_)) %>% 
  filter(!is.na(tipo)) %>% 
  group_by(inegi, sexo, tipo) %>% 
  summarise(total = n()) %>% 
  ungroup() %>% 
  mutate(tipo = case_when(sexo == 1 ~ paste0(tipo, "H"),
                          sexo == 2 ~ paste0(tipo, "M"),
                          T ~ NA_character_)) %>% 
  select(-sexo) %>% 
  pivot_wider(names_from = tipo, values_from = total) %>% 
  right_join(pob_mun %>% select(inegi)) %>% 
  replace_na(list(cancerH = 0, cancerM = 0,
                  homicidioH = 0, homicidioM = 0,
                  suicidioH = 0, suicidioM = 0))

rm(pattern_cancer)

# ---- Censo ----
censo <- left_join(
  readRDS(files$censo_per) %>% 
    mutate(inegi = paste0(ent, mun),
           edad = ifelse(edad==999, NA, edad),
           escoacum = ifelse(escoacum==99, NA, escoacum),
           muj_0a17 = ifelse(sexo==3 & edad<18, 1, 0),
           nini = ifelse(conact>20 & conact!=50, 1, 0),
           muj_0a17_nent = ifelse(muj_0a17==1 & nini==1, 1, 0),
           mas50k = ifelse(tamloc>3, 1, 0)) %>% 
    group_by(inegi) %>% 
    summarise(escoacum = weighted.mean(escoacum, factor, na.rm=T),
              edad = weighted.mean(edad, factor, na.rm=T),
              muj_0a17_nent = sum(muj_0a17_nent*factor, na.rm = T),
              muj_0a17 = sum(muj_0a17*factor, na.rm = T),
              pct_muj_0a17_nent = muj_0a17_nent*(100/muj_0a17),
              mas50k = sum(mas50k*factor, na.rm = T),
              pct_mas50k = mas50k*(100/sum(factor, na.rm = T))) %>% 
    ungroup(),
  readRDS(files$censo_viv) %>%
    mutate(inegi = paste0(ent, mun),
           jefe_fem = ifelse(jefe_sexo==3, 1, 0)) %>%
    group_by(inegi) %>%
    summarise(jefe_fem = sum(jefe_fem*factor, na.rm = T),
              tot_viv = sum(factor, na.rm = T),
              pct_jefe_fem = jefe_fem*(100/tot_viv)) %>% 
    ungroup()
  
)

# ---- RNPDNO ----

rnpdno <- readRDS(files$rnpdno) %>% 
  filter(nom_mun != "Se Desconoce" & nom_mun != "Sin Municipio De Referencia") %>% 
  mutate(nom_mun = clean_text(nom_mun)) %>% 
  right_join(pob_mun %>% 
               select(inegi, nom_mun) %>% 
               mutate(cve_ent = str_sub(inegi, 1, 2),
                      nom_mun = clean_text(nom_mun))) %>% 
  select(inegi, Hombres, Mujeres, Indeterminado) %>% 
  rename(desapH = Hombres,
         desapM = Mujeres,
         desapI = Indeterminado) %>% 
  replace_na(list(desapH = 0, desapM = 0, desapI = 0))

# ---- Militares ----

militares <- readRDS(files$militares) %>% 
  mutate(inegi = paste0(cve_ent, cve_mun)) %>% 
  select(-c(cve_ent, cve_mun)) %>% 
  select(inegi, everything())

# ---- Incidencia ----

incidencia <- readRDS(files$incidencia) %>% select(-pob)

# ---- Materna ----

materna <- readRDS(files$materna) %>% 
  filter(ent_ocurr!="99" & mun_ocurr!="999" & razon_m==1) %>% 
  mutate(inegi = paste0(ent_ocurr, mun_ocurr)) %>% 
  group_by(inegi) %>% 
  summarise(m_materna = n()) %>% 
  ungroup() %>% 
  right_join(pob_mun %>% 
               select(inegi)) %>% 
  replace_na(list(m_materna = 0))

# ---- DENUE ----
denue <- pob_mun %>% 
  select(inegi) %>% 
  left_join(readRDS(files$denue_agro)) %>% 
  left_join(readRDS(files$denue_min)) %>% 
  left_join(readRDS(files$denue_tex)) %>% 
  replace_na(list(tot_agr = 0,
                  tot_min = 0,
                  tot_text = 0))

# ---- Conflictos ----

conflictos <- readRDS(files$conflictos)

# ---- Centros de atención ----

centros <- readRDS(files$centros_atencion) %>% select(-pob)

# ---- Accesibilidad por carreteras ----

carreteras <- readRDS(files$carreteras)

# ---- Base completa ----

main_df <- pob_mun %>% 
  left_join(sinais) %>%
  left_join(censo) %>% 
  left_join(rnpdno) %>% 
  left_join(militares) %>% 
  left_join(incidencia) %>% 
  left_join(materna) %>% 
  left_join(denue) %>% 
  left_join(conflictos) %>% 
  left_join(centros) %>% 
  left_join(carreteras) %>% 
  mutate(tasa_homicidioH = homicidioH*(100000/pobH),
         tasa_suicidioH = suicidioH*(100000/pobH),
         tasa_cancerH = cancerH*(100000/pobH),
         tasa_homicidioM = homicidioM*(100000/pobM),
         tasa_suicidioM = suicidioM*(100000/pobM),
         tasa_cancerM = cancerM*(100000/pobM),
         tasa_desapH = desapH*(100000/pobH),
         tasa_desapM = desapM*(100000/pobM),
         tasa_m_materna = m_materna*(100000/pobM),
         tasa_agro = tot_agr*(100000/(pobH+pobM)),
         tasa_min = tot_min*(100000/(pobH+pobM)),
         tasa_text = tot_text*(100000/(pobH+pobM)))

saveRDS(main_df, files$output)

# done.

# Elaboración de Análisis Estadísticos sobre Violencia Feminicida y Factores Geoespaciales Asociados

El presente repositorio contiene los códigos, accesos y bases de datos - tanto crudas como procesadas - utilizadas por _Data Cívica_ como parte del proyecto de consultoría para ONU Mujeres México. Los códigos se encuentran en el lenguaje estadístico _R_, y están hechos para ser ejecutados individualmente o como un proceso completo por medio de _Makefiles_. Estos últimos archivos son ejecutables directamente en la terminal UNIX, ubicandose en la carpeta del task elegido y ejecutando el comando _make_.

Adicionalmente, en análisis está ordenado y dividido en tareas o _tasks_, organizadas en carpetas separadas que contienen cada una las siguentes carpetas:
- __input:__ archivos _externos_ u originales que no han tenido ningún tipo de procesamiento.
- __src__: _scripts_/códigos que nos llevan de los _inputs_ a los _outputs_.
- __output:__ archivos resultado del procesamiento de los códigos contenidos en __src__

Por lo anterior, siempre el flujo de las carpetas es _input_ --> _src_ --> _outputs_. Los _inputs_ de las tareas subsecuentes al _import_ pueden ser los _outputs_ o resultados de las tareas previas.

Los _tasks_ tienen el siguiente orden

### import

En esta sección consiste en importar las bases de datos de formatos de texto plano a un formato _.rds_. Esto tiene dos fines, por un lado el hacer una primera limpieza de las bases (tirar variables no necesarias, limpieza de nombres de variables filtrar observaciones necesarias), y por el otro conservar las bases de datos originales intactas, pudiendo regresar a la versión original en caso de existir algún problema.

### clean-data

Aquí es donde se limpian y procesan los datos, con el fin de tener una base de datos única de la que puedan desprenderse los distintos análisis que contiene este proyecto.

### local-moran

Aquí se encuentra todo lo correspondiente al análisis de autocorrelación espacial realizado con el método _local moran_.

### descriptives

En esta sección se encuentra todo lo correspondiente al análisis exploratorio de datos (_EDA_), incluyendo gráficas descriptivas y algunos otros estadísticos y pruebas estadísticas.

### model

En esta última sección está todo lo relacionado al análisis econométrico, incluyendo modelos y pruebas post-estimación.

Para dudas, contactar a info@datacivica.org
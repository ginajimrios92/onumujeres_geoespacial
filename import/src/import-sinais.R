#
# Author: OE
# Maintainer(s): OE
# Copyright: 2021 © Data Cívica, GPL v2 or newer
# --------------------------------------------------
# onumujeres_geoespacial/import/src/import-sinais.R
#

pacman::p_load(foreign, tidyverse, janitor, here)

files <- list(input = here("import/input/DEFUN19.dbf"),
              cat = here("import/input/cat-causas.rds"),
              output = here("import/output/sinais.rds")
              )

sinais <- read.dbf(files$input, as.is = T) %>% 
  clean_names() %>% 
  mutate(inegi = paste0(ent_ocurr, mun_ocurr)) %>% 
  rename(year = anio_ocur) %>% 
  select(inegi, year, sexo, presunto, causa_def) %>% 
  left_join(readRDS(files$cat))
  
saveRDS(sinais, files$output)

# done.


